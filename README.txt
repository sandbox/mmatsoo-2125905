Auto Abbreviate module.

A module that automatically puts <abbr> tags around acronyms defined by admins. For example, an admin defines HTML as "HyperText Markup Language" and instances of "HTML" automatically become <abbr title="HyperText Markup Language">HTML</abbr> in the node's body content. You can define which content types use this functionality.

Example:
You write a lot of Drupal tutorials and you have created a "tutorial" content type. You find you're always using terms like HTML, CSS, PHP and would like to have them wrapped in <abbr> tags. (ex. <abbr title="Cascading Style Sheets">CSS</abbr>) You add their definitions using the Auto Abbreviate module and by enabling its use on the "tutorial" content type, all your tutorials will have instances of HTML, CSS and PHP wrapped in <abbr> tags.

Install it like you would any other module, by enabling it on the modules UI or through drush.

Configuration at admin/config/auto-abbreviate.

Notes:
The logic to pick out targeted acronyms in the node body is not the most robust. It currently uses str_ireplace() instead of a regular expression. So if your acronym is not wrapped in any other html tags (ex. <strong>, <a>) then it should work okay.